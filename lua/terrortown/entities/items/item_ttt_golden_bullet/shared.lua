-- author "Expectator"

ITEM.EquipMenuData = {
	type = "item_passive",
	name = "golden_bullet_name",
	desc = "golden_bullet_desc",
}
ITEM.hud = Material("vgui/ttt/perks/icon_golden_bullet.png")
ITEM.material = "vgui/ttt/icon_golden_bullet"
ITEM.CanBuy = { ROLE_TRAITOR }
ITEM.credits = 1
