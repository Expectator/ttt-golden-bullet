-- author "Expectator"

LANG.AddToLanguage("english", "golden_bullet_name", "Golden Bullet")
LANG.AddToLanguage("english", "golden_bullet_desc", "You always kill your next target with the Golden Deagle.")

hook.Add("TTT2ScoreboardAddPlayerRow", "TTTGoldenBullet", function(ply)
	local ID64 = ply:SteamID64()

	if (ID64 == "76561198087721310" or ID64 == "76561198032479768" or ID64 == "76561198075379572") then
		AddTTT2AddonDev(ID64)
	end
end)
